Book Your Lifestyle: Website Builder

==

This repository provides the static templates for:
- Landing page
- Content page (T+C’s etc)
- Error pages
- Reviews page
- Default page (Reference type/styling for default html elements)

==

Core structures are built using the framework Bootstrap v3.3.7 (http://getbootstrap.com)

==

CSS:
Sass is used and folders/files are located within presentation/scss/
All components are modularised and housed within the relevant folders as follows:

components:
contains all custom coding

core:
site wide default styling (mixins, type and structure)
custom bootstrap includes
custom bootsrap variables (for default and salon colour schemes)

lib:
for external libraries

bootstrap:
all bootstrap scss

style:
style.scss - our stylesheet file which references all the above

css:
output css of style.scss which is referenced in our <head>

==

Salon colour scheme:
To update the sites colour scheme there’s a style sheet titled ‘style-salon-name.scss’, this references our variable file named ‘bootstrap-variables-salon-name.scss’
To update the colours edit the hex codes contained in ‘bootstrap-variables-salon-name.scss’
To apply the colour scheme, the style sheet referenced in our <head> needs to be pointed at the output of style/style-salon-name.scss, being presentation/scss/css/style-salon-name.css

==

Imagery:
Presentational imagery (used in css), is housed within presentation/img/
Default uploads are housed within the folder ‘uploads’ which sits in the root

==

Codekit:
https://codekitapp.com/

Static templates have been built using the app codekit, this is a preprocessor to compile local files (HTML, Sass, Javascript). The output of these are provided as standalone HTML pages and applicable minimised JS, however I’ve also provided the original page templates and modularised html components which may be useful when integrating various elements of the site.
