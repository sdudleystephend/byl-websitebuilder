//single column toggle menu
(function($){
  $(function(options){
    var settings = $.extend({
      "wrapper": "#branding",
      "navToggle": "#toggleNav",
      "navToggleActiveClass": "active",
      "navList": "nav > .navwrap",
      "animationSpeed": "normal",
      "childClass": "hasChild",
      "childToggleClass": "toggleSub",
      "childToggleTitle": "toggle",
      "childToggleClassActive": "open",
      "breakpoint": "768"
    }, options );
    //console.log(settings);
    $(settings.navToggle).click(function(e) {
      $(settings.wrapper).find(settings.navList).slideToggle(settings.animationSpeed);
      $(settings.wrapper).find(settings.navToggle).toggleClass(settings.navToggleActiveClass);
      e.preventDefault();
    });
    $(settings.wrapper).find('li').has('ul').addClass(settings.childClass).prepend('<a href="#toggle" class="' + settings.childToggleClass + '">+</a>');
    $('.' + settings.childToggleClass).click(function (){
      $(this).toggleClass(settings.childToggleClassActive).find('~ ul').slideToggle();
      if ($(this).html() == '-') {
        $(this).html('+');
      } else {
        $(this).html('-');
      }
      return;
    });
    $(window).resize(function(){
      if ($(window).width() > settings.breakpoint ) {
        $(settings.wrapper).find('ul').removeAttr("style");
        $(settings.wrapper).find('a.' + settings.childToggleClassActive).removeClass(settings.childToggleClassActive);
        $('.' + settings.childToggleClass).html('+');
      }
    });
  });
}(jQuery));

//preload
$(window).load(function() {
  $("body").removeClass("preload");
});

//accordion active class
$(document).ready(function() {
  $('.panel-collapse').on('show.bs.collapse', function () {
    $(this).siblings('.panel-title').addClass('active');
  });

  $('.panel-collapse').on('hide.bs.collapse', function () {
    $(this).siblings('.panel-title').removeClass('active');
  });
});
