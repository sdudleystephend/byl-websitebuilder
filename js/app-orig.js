//single column toggle menu
(function($){
  $(function(options){
    var settings = $.extend({
      "wrapper": "#branding",
      "navToggle": "#toggleNav",
      "navToggleActiveClass": "active",
      "navList": "nav > .navwrap",
      "animationSpeed": "normal",
      "childClass": "hasChild",
      "childToggleClass": "toggleSub",
      "childToggleTitle": "toggle",
      "childToggleClassActive": "open",
      "breakpoint": "768"
    }, options );
    //console.log(settings);
    $(settings.navToggle).click(function(e) {
      $(settings.wrapper).find(settings.navList).slideToggle(settings.animationSpeed);
      $(settings.wrapper).find(settings.navToggle).toggleClass(settings.navToggleActiveClass);
      e.preventDefault();
    });
    $(settings.wrapper).find('li').has('ul').addClass(settings.childClass).prepend('<a href="#toggle" class="' + settings.childToggleClass + '">+</a>');
    $('.' + settings.childToggleClass).click(function (){
      $(this).toggleClass(settings.childToggleClassActive).find('~ ul').slideToggle();
      if ($(this).html() == '-') {
        $(this).html('+');
      } else {
        $(this).html('-');
      }
      return;
    });
    $(window).resize(function(){
      if ($(window).width() > settings.breakpoint ) {
        $(settings.wrapper).find('ul').removeAttr("style");
        $(settings.wrapper).find('a.' + settings.childToggleClassActive).removeClass(settings.childToggleClassActive);
        $('.' + settings.childToggleClass).html('+');
      }
    });
  });
}(jQuery));

//preload
$(window).load(function() {
  $("body").removeClass("preload");
});

//owl carousel split image
$(document).ready(function() {
  $('.owl-carousel.owl-leader').owlCarousel({
    margin:1,
    loop: true,
    lazyLoad: true,
    autoplay:false,
    autoHeight:true,
    center:false,
    nav:false,
    dots: false,
    responsive:{
      0:{
        items:1
      },
      480:{
        items:2,
        center:true
      },
      768:{
        items:3,
        center:true
      }
    }
  })
})

//owl carousel blockquote
$(document).ready(function() {
  $('.owl-carousel.owl-blockquote').owlCarousel({
    margin:0,
    loop: true,
    lazyLoad: true,
    autoplay:true,
    animateOut: 'fadeOut',
    mouseDrag:false,
    autoHeight:true,
    center:true,
    nav:false,
    dots: false,
    responsive:{
      0:{
        items:1
      }
    }
  })
})

//owl carousel imagery strip
$(document).ready(function() {
  $('.owl-carousel.owl-imagery-strip').owlCarousel({
    margin:0,
    loop: true,
    lazyLoad: true,
    autoplay:true,
    autoplayTimeout:3500,
    slideBy: 3,
    mouseDrag:true,
    autoHeight:false,
    center:false,
    nav:false,
    dots: false,
    autoWidth:true,
    responsive:{
      0:{
        items:1
      },
      480:{
        items:2,
        center:false
      },
      1024:{
        items:3,
        center:true
      },
      1400:{
        items:4
      }
    }
  })
})

//owl carousel instagram
$(document).ready(function() {
  $('.owl-carousel.owl-instagram').owlCarousel({
    margin:30,
    loop: true,
    lazyLoad: true,
    autoplay:true,
    autoplayTimeout:3500,
    slideBy: 3,
    mouseDrag:true,
    autoHeight:false,
    center:false,
    nav:false,
    dots: false,
    autoWidth:true,
    responsive:{
      0:{
        items:1
      },
      480:{
        items:2,
        center:false
      },
      1024:{
        items:3,
        center:true
      },
      1400:{
        items:4
      }
    }
  })
})


//accordion active class
$(document).ready(function() {
  $('.panel-collapse').on('show.bs.collapse', function () {
    $(this).siblings('.panel-title').addClass('active');
  });

  $('.panel-collapse').on('hide.bs.collapse', function () {
    $(this).siblings('.panel-title').removeClass('active');
  });
});

/* Add shrink class to header */
jQuery(document).ready(function($) {
  jQuery(window).on('scroll touchmove', function () {
    jQuery('body').toggleClass('shrink', $(document).scrollTop() > 0);
    jQuery('#branding').toggleClass('shrink', $(document).scrollTop() > 0);
  }).scroll();
});