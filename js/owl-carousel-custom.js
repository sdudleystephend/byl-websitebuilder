$(document).ready(function() {
  $('#slider-carousel').owlCarousel({
    margin: 30,
    slideBy: 3,
    center:true,
    nav:false,
    dots: false,
    lazyLoad: true,
    autoplay:true,
    autoplayTimeout:3500,
    loop: true,
    autoWidth: true,
    rtl:false,
    responsive:{
      0:{
        items:1,
        autoWidth: true,
      },
      480:{
        items:2,
        autoWidth: true
      },
      1080:{
        items:4,
        autoWidth: true
      }
    }
  })
})

$(document).ready(function() {
  $('#slider-imagery-strip').owlCarousel({
    margin:0,
    loop: true,
    lazyLoad: true,
    autoplay:true,
    autoplayTimeout:3500,
    slideBy: 1,
    mouseDrag:true,
    autoWidth: true,
    center:true,
    nav:false,
    dots: false,
    rtl:false,
    responsive:{
      0:{
        items:2,
        slideBy: 1,
        autoWidth: true,
        rtl:false
      },
      480:{
        items:2,
        slideBy: 3,
        center:false,
        autoWidth: true,
        rtl:false
      },
      1024:{
        items:3,
        slideBy: 3,
        center:true,
        autoWidth: true,
        rtl:false
      },
      1400:{
        items:4,
        slideBy: 3,
        autoWidth: true,
        rtl:false
      }
    }
  })
})

$(document).ready(function() {
  $('#slider-blockquote').owlCarousel({
    margin:0,
    loop: true,
    lazyLoad: true,
    autoplay:true,
    animateOut: 'fadeOut',
    mouseDrag:false,
    autoHeight:true,
    center:true,
    nav:false,
    dots: false,
    responsive:{
      0:{
        items:1
      }
    }
  })
})

$(document).ready(function() {
  $('#slider-hero').owlCarousel({
    margin:1,
    loop: true,
    lazyLoad: true,
    autoplay:false,
    autoHeight:false,
    center:false,
    nav:false,
    dots: false,
    rtl:false,
    responsive:{
      0:{
        items:1,
        rtl:false
      },
      480:{
        items:2,
        center:true,
        rtl:false
      },
      768:{
        items:3,
        center:true,
        rtl:false
      }
    }
  })
})

